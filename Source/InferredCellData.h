
#pragma once

using encounter_list_t = PackableListWithJson<uint16_t>;
using encounter_map_t = PackableHashTableWithJson<DWORD, encounter_list_t>;

class CInferredCellData : public InferredData
{
public:
	void Init();
	
	CLandBlockExtendedData *GetLandBlockData(DWORD landblock);

	//uint16_t GetTerrain(DWORD landcell)
	//{
	//	long x, y;
	//	return LandDefs::gid_to_lcoord(landcell, x, y) ? GetTerrain(landcell, x, y) : 0;
	//}

	//uint16_t GetTerrain(DWORD landblock, long cellx, long celly)
	//{

	//}

	uint16_t GetEncounterIndex(DWORD landcell)
	{
		long x, y;
		return LandDefs::gid_to_lcoord(landcell, x, y) ? GetEncounterIndex(landcell >> 16, x, y) : 0;
	}
	uint16_t GetEncounterIndex(DWORD landblock, long cellx, long celly)
	{
		encounter_map_t::iterator itr = _encounters.find(landblock);
		if (itr != _encounters.end())
		{
			int index = (cellx * 9) + celly;
			return *(itr->second.GetAt(index));
		}

		uint16_t terrain = _data.get_terrain(landblock << 16, cellx, celly);
		return (terrain >> 7) & 0xF;
	}

protected:

	CLandBlockExtendedDataTable _data;
	CLandBlockExtendedDataTable _jsonData;

	encounter_map_t _encounters;
};

