
#include "StdAfx.h"
#include "PhatSDK.h"
#include "InferredCellData.h"

void CInferredCellData::Init()
{
#ifndef PUBLIC_BUILD
	SERVER_INFO << "Loading inferred cell data...";
#endif

	_data.Destroy();

	LoadCacheData(6, 0xcd57fd07, 0x697a2224, _data);

	std::filesystem::path dataPath("data/json");

	LoadJsonData(dataPath / "worldspawns.json", _jsonData);

	LoadJsonData(dataPath / "encounters.json", _encounters);

	//BYTE *data = NULL;
	//DWORD length = 0;
	//if (LoadDataFromPhatDataBin(6, &data, &length, 0xcd57fd07, 0x697a2224)) // lbed.bin
	//{
	//	BinaryReader reader(data, length);
	//	_data.UnPack(&reader);
	//	delete[] data;
	//}

	//std::ifstream fileStream("data\\json\\worldspawns.json");

	//if (fileStream.is_open())
	//{
	//	json jsonData;
	//	fileStream >> jsonData;
	//	fileStream.close();

	//	_jsonData.UnPackJson(jsonData);
	//}

#ifndef PUBLIC_BUILD
	SERVER_INFO << "Finished loading inferred cell data ("
		<< (DWORD)_data.landblocks.size() << "and" << (DWORD) _jsonData.landblocks.size()
		<< ") encounters" << _encounters.size() << "...";
#endif
}

CLandBlockExtendedData *CInferredCellData::GetLandBlockData(DWORD landblock)
{
	CLandBlockExtendedData *data = _jsonData.landblocks.lookup(landblock);

	if (!data)
	{
		data = _data.landblocks.lookup(landblock);
	}

	return data;
}
